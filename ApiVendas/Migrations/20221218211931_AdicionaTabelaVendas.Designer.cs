﻿// <auto-generated />
using System;
using ApiVendas.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

#nullable disable

namespace ApiVendas.Migrations
{
    [DbContext(typeof(OrganizadorContext))]
    [Migration("20221218211931_AdicionaTabelaVendas")]
    partial class AdicionaTabelaVendas
    {
        /// <inheritdoc />
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder.HasAnnotation("ProductVersion", "7.0.1");

            modelBuilder.Entity("ApiVendas.Models.Venda", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<string>("CpfVendedor")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("Data")
                        .HasColumnType("TEXT");

                    b.Property<string>("EmailVendedor")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<int>("IdVendedor")
                        .HasColumnType("INTEGER");

                    b.Property<string>("Itens")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<string>("NomeVendedor")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<string>("Status")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<string>("TelefoneVendedor")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.ToTable("Vendas");
                });
#pragma warning restore 612, 618
        }
    }
}
