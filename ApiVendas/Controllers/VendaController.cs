using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using ApiVendas.Context;
using ApiVendas.Models;

namespace ApiVendas.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly OrganizadorContext _context;

        public VendaController(OrganizadorContext context)
        {
            _context = context;
        }

        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            // Buscar o Id no banco utilizando o EF
            // Validar o tipo de retorno. Se não encontrar a venda, retornar NotFound,
            // caso contrário retornar OK com a venda encontrada

            var venda = _context.Vendas.Find(id);

            if (venda == null)
                return NotFound();

            return Ok(venda);

        }

        [HttpGet("ObterTodas")]
        public IActionResult ObterTodas()
        {
            var vendas = _context.Vendas.ToList();
            return Ok(vendas);
        }

        [HttpPost]
        public IActionResult Criar(Venda venda)
        {
            if (venda.Itens == null || venda.Itens == "")
                return BadRequest(new { Erro = "É necessário pelo menos um item de venda!" });

            // Adicionar a venda recebida no EF e salvar as mudanças (save changes)
            venda.Status = "Aguardando_pagamento";
            _context.Vendas.Add(venda);
            _context.SaveChanges();

            return CreatedAtAction(nameof(ObterPorId), new { id = venda.Id }, venda);
        }

        [HttpPut("{id}")]
        public IActionResult Atualizar(int id, Venda venda)
        {
            var vendaBanco = _context.Vendas.Find(id);

            string mensagem = "Atualização não permitida. Status(Aguardando_pagamento) só pode ser atualizado para (Pagamento_aprovado) ou (Cancelada). Status(Pagamento_aprovado) só pode ser atualizado para (Enviado_para_transportadora) ou (Cancelada). Status(Enviado_para_transpotadora) só pode ser atualizado para (Entregue)";


            if (vendaBanco == null)
                return NotFound();

            //Deixado de propósito iguais ao banco, com exeção do Status//
           
            vendaBanco.IdVendedor = vendaBanco.IdVendedor;
            vendaBanco.CpfVendedor = vendaBanco.CpfVendedor;
            vendaBanco.NomeVendedor = vendaBanco.NomeVendedor;
            vendaBanco.EmailVendedor = vendaBanco.EmailVendedor;
            vendaBanco.Data = vendaBanco.Data;
            vendaBanco.Itens = vendaBanco.Itens;

            if ((vendaBanco.Status == "Aguardando_pagamento" && venda.Status == "Pagamento_aprovado") || (vendaBanco.Status == "Aguardando_pagamento" && venda.Status == "Cancelada") || (vendaBanco.Status == "Pagamento_aprovado" && venda.Status == "Enviado_para_transportadora") || (vendaBanco.Status == "Pagamento_aprovado" && venda.Status == "Cancelada") || (vendaBanco.Status == "Enviado_para_transportadora" && venda.Status == "Entregue"))
            {
                vendaBanco.Status = venda.Status;
                _context.Vendas.Update(vendaBanco);
                _context.SaveChanges();
                return Ok();
            }
            
            else
            return BadRequest(new { Erro = mensagem });
        }

        [HttpDelete("{id}")]
        public IActionResult Deletar(int id)
        {
            var vendaBanco = _context.Vendas.Find(id);

            if (vendaBanco == null)
                return NotFound();

            // Remover a venda encontrada através do EF e salvar as mudanças (save changes)*/

            _context.Vendas.Remove(vendaBanco);
            _context.SaveChanges();

            return NoContent();
        }
    }
}
